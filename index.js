#!/usr/bin/env node

const { createServer } = require("http");
const { join } = require("path");
const { readFileSync } = require("fs");

const { RushConfiguration } = require("@microsoft/rush-lib");
const yargs = require("yargs");

function graphEntry({ packageName, dependencyGraph }) {
  dependencyGraph[packageName] = dependencyGraph[packageName] || {
    dependencies: [],
    dependents: [],
  };

  return dependencyGraph[packageName];
}

function addDependencies({
  project,
  rushConfiguration,
  dependencyGraph,
  dependencies = {},
}) {
  const entry = graphEntry({
    dependencyGraph,
    packageName: project.packageName,
  });

  Object.keys(dependencies).forEach((dependencyName) => {
    if (
      rushConfiguration.projectsByName.has(dependencyName) &&
      !project.cyclicDependencyProjects.has(dependencyName)
    ) {
      entry.dependencies.push(dependencyName);

      const dependencyEntry = graphEntry({
        dependencyGraph,
        packageName: dependencyName,
      });
      dependencyEntry.dependents.push(project.packageName);
    }
  });
}

function processPackage({ dependencyGraph, project, rushConfiguration }) {
  addDependencies({
    project,
    rushConfiguration,
    dependencyGraph,
    dependencies: {
      ...project.packageJson.dependencies,
      ...project.packageJson.devDependencies,
      ...project.packageJson.peerDependencies,
    },
  });
}

function makeDependencyGraph(startingFolder) {
  const dependencyGraph = {};
  const rushConfiguration = RushConfiguration.loadFromDefaultLocation({
    startingFolder,
  });

  rushConfiguration.projects.forEach((project) =>
    processPackage({
      project,
      dependencyGraph,
      rushConfiguration,
    })
  );

  return dependencyGraph;
}

function main() {
  const options = yargs
    .usage("Usage: -r <root> -p <port> -s")
    .option("r", {
      alias: "root",
      describe: "Path to a rush project",
      type: "string",
      default: process.cwd(),
    })
    .option("d", {
      alias: "dump",
      describe: "Dump raw data",
      type: "boolean",
    })
    .option("p", {
      alias: "port",
      describe: "Port to run a localhost server on",
      type: "number",
      default: 8080,
    })
    .option("s", {
      alias: "serve",
      describe: "Serve a generated graph",
      type: "boolean",
    }).argv;

  const dependencyGraph = JSON.stringify(
    makeDependencyGraph(options.root),
    null,
    2
  );

  if (options.dump) {
    console.log(dependencyGraph);
    return;
  }

  const template = readFileSync(join(__dirname, "index.tpl")).toString();
  const script = readFileSync(join(__dirname, "script.js")).toString();
  const style = readFileSync(join(__dirname, "style.css")).toString();
  const out = template
    .replace("<<data>>", dependencyGraph)
    .replace("<<script>>", script)
    .replace("<<style>>", style);

  if (options.serve) {
    const hostname = "127.0.0.1";
    const server = createServer((req, res) => {
      res.statusCode = 200;
      res.setHeader("Content-Type", "text/html");
      res.end(out);
    });

    server.listen(options.port, hostname, () => {
      console.log(`Visit http://${hostname}:${options.port}/`);
    });
  } else {
    console.log(out);
  }
}

main();
