function randomColor() {
  const colors = [
    "#e6194B",
    "#3cb44b",
    "#4363d8",
    "#f58231",
    "#42d4f4",
    "#f032e6",
    "#469990",
    "#dcbeff",
    "#9A6324",
    "#800000",
    "#aaffc3",
    "#000075",
    "#a9a9a9",
  ];
  let counter = 0;
  function nextColor() {
    return colors[++counter % colors.length];
  }
  return nextColor;
}

const styles = [
  [
    {
      selector: "node",
      style: {
        "background-color": (e) => e.data("color"),
        label: "data(id)",
        width: (e) => e.data("dependencies") * 5 + 15,
        height: (e) => e.data("dependencies") * 5 + 15,
      },
    },

    {
      selector: "edge",
      style: {
        width: 3,
        "line-color": (e) => e.data("dependentsColor"),
        "target-arrow-color": (e) => e.data("dependentsColor"),
        "target-arrow-shape": "triangle",
        "curve-style": "bezier",
      },
    },
  ],
  [
    {
      selector: "node",
      style: {
        "background-color": (e) => e.data("color"),
        label: "data(id)",
        width: (e) => e.data("dependencies") * 5 + 15,
        height: (e) => e.data("dependencies") * 5 + 15,
      },
    },

    {
      selector: "edge",
      style: {
        width: 3,
        "line-color": (e) => e.data("dependenciesColor"),
        "target-arrow-color": (e) => e.data("dependenciesColor"),
        "target-arrow-shape": "triangle",
        "curve-style": "bezier",
      },
    },
  ],
];

const layouts = {
  breadthfirst: {
    name: "breadthfirst",
    directed: true,
    maximal: true,
    avoidOverlap: true,
  },
  concentric: {
    name: "concentric",
    concentric: function (node) {
      return node.data("dependents") / node.data("dependencies");
    },
    directed: true,
    avoidOverlap: true,
  },
  concentric_inverted: {
    name: "concentric",
    concentric: function (node) {
      return node.data("dependencies") / node.data("dependents");
    },
    directed: true,
    avoidOverlap: true,
  },
};

const elements = [];
const colors = {};
const nextColor = randomColor();
Object.keys(data).forEach((id) => {
  colors[id] = nextColor();
});

Object.entries(data).forEach(([id, v]) => {
  const links = v.dependencies.length + v.dependents.length;
  if (v.dependencies.length + v.dependents.length === 0) return;

  elements.push({
    data: {
      id,
      color: colors[id],
      dependencies: v.dependencies.length,
      dependents: v.dependents.length,
    },
  });

  v.dependencies.forEach((dep) => {
    elements.push({
      data: {
        id: `${id}--${dep}`,
        source: id,
        target: dep,
        dependenciesColor: colors[id],
        dependentsColor: colors[dep],
      },
    });
  });
});

function main() {
  var cy = cytoscape({
    container: document.getElementById("cy"),
    elements,
    style: styles[0],
    layout: layouts[document.getElementById("layout-select").value],
  });

  document
    .getElementById("color-select")
    .addEventListener("change", (ev) =>
      cy.style(styles[ev.target.checked ? 0 : 1])
    );

  document
    .getElementById("layout-select")
    .addEventListener("change", (ev) =>
      cy.layout(layouts[ev.target.value]).run()
    );
}

main();
