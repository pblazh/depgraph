<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Dep Graph</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cytoscape/3.19.0/cytoscape.min.js"></script>
    <style>
    <<style>>
    </style>
  </head>
  <body>
    <div id="cy"></div>
    <div class="seelectors">
      <label>
        <input type="checkbox" id="color-select" checked>
        revert dependencies color
      </label>
      <select  id="layout-select" value="breadthfirst">
        <option value="breadthfirst">breadthfirst</option>
        <option value="concentric">concentric</option>
        <option value="concentric_inverted">inverted concentric</option>
      </select>
    </div>

    <script>
      const data = <<data>>
      <<script>>
    </script>
  </body>
</html>
